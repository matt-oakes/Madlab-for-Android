package com.matto1990.app.madlab.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.matto1990.app.madlab.R;
import com.matto1990.app.madlab.provider.MadlabContract.Events;
import com.matto1990.app.madlab.provider.MadlabContract.Meetups;
import com.matto1990.app.madlab.util.TitlebarActivity;

public class HomeActivity extends TitlebarActivity {
    /** State held between configuration changes. */
    private Button eventsButton;
    private Button calendarButton;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        
        // Setup the title bar UI
        setupTitlebar();
        
        // Get the views
        eventsButton = (Button) findViewById(R.id.dash_btn_events);
        calendarButton = (Button) findViewById(R.id.dash_btn_calendar);
        
        // Set the events button action
        eventsButton.setOnClickListener(eventsListener);
        // Set the calendar button action
        calendarButton.setOnClickListener(calendarListener);
    }
    
    // onClick run when the events button is pressed
    private final OnClickListener eventsListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            final Intent intent = new Intent(Intent.ACTION_VIEW, Events.CONTENT_URI);
            intent.putExtra(STATE, state);
            startActivity(intent);
        }
    };
    
    // onClick run when the events button is pressed
    private final OnClickListener calendarListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            final Intent intent = new Intent(Intent.ACTION_VIEW, Meetups.CONTENT_URI);
            intent.putExtra(STATE, state);
            startActivity(intent);
        }
    };
}