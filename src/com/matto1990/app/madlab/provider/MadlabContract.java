package com.matto1990.app.madlab.provider;

import java.util.regex.Pattern;

import android.content.ContentProvider;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * This class contain information and methods about the content uri's used by the {@link MadlabProvider}
 */
public class MadlabContract {
    // The content uri's that are used to gain access to the content providers data
    public static final String CONTENT_AUTHORITY = "com.matto1990.app.madlab";
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    
    // Some constants used to create content uri's
    private static final String PATH_EVENTS = "events";
    private static final String PATH_MEETUPS = "meetups";
    
    /*
     * Columns that relate to madlab events
     */
    interface EventColumns {
        String EVENT_ID = "event_id";
        String EVENT_TITLE = "event_title";
        String EVENT_DESCRIPTION = "description";
        String EVENT_INTERESTED = "interested";
    }
    
    /*
     * Columns that relate to madlab meetups
     */
    interface MeetupColumns {
        String MEETUP_ID = "meetup_id";
        String EVENT_ID = "meetup_event_id";
        String MEETUP_TITLE = "meetup_title";
        String MEETUP_DESCRIPTION = "description";
        String MEETUP_LOCATION = "location";
        String MEETUP_ATTENDING = "attending";
        String MEETUP_START_DATETIME = "start_datetime";
        String MEETUP_END_DATETIME = "end_datetime";
    }
    
    /**
     * Events are things which happen regually at madlab. Normally every week, month or fortnight.
     * They normally have meetups associated with them.
     * 
     * @author oakesm9
     */
    public static class Events implements EventColumns, BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_EVENTS).build();

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.madlab.event";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.madlab.event";

        public static final String DEFAULT_SORT = EventColumns.EVENT_ID + " ASC";
        
        public static Uri buildEventUri(String eventId) {
            return CONTENT_URI.buildUpon().appendPath(eventId).build();
        }

        public static Uri buildMeetupsUri(String eventId) {
            return CONTENT_URI.buildUpon().appendPath(eventId).appendPath(PATH_MEETUPS).build();
        }
        
        public static String getEventId(Uri uri) {
            return uri.getPathSegments().get(1);
        }
        
        public static String generateEventId(String eventTitle) {
            return sanitizeId(eventTitle, false);
        }
    }
    
    public static class Meetups implements MeetupColumns, BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_MEETUPS).build();

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.madlab.meetup";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.madlab.meetup";

        public static final String DEFAULT_SORT = MeetupColumns.MEETUP_START_DATETIME + " ASC";
        
        public static Uri buildMeetupUri(String meetupId) {
            return CONTENT_URI.buildUpon().appendPath(meetupId).build();
        }
        
        public static String getMeetupId(Uri uri) {
            return uri.getPathSegments().get(1);
        }
        
        public static String generateMeetupId(String meetupTitle, String startDatetime) {
            return sanitizeId(meetupTitle + "-" + startDatetime, false);
        }
    }
    
    
    /** Used to sanitize a string to be {@link Uri} safe. */
    private static final Pattern sSanitizePattern = Pattern.compile("[^a-z0-9-_]");
    private static final Pattern sParenPattern = Pattern.compile("\\(.*?\\)");
    /**
     * Sanitize the given string to be {@link Uri} safe for building
     * {@link ContentProvider} paths.
     */
    public static String sanitizeId(String input, boolean stripParen) {
        if (input == null) return null;
        
        // Remove /'s
        input = input.replaceAll("/", "");
        
        if (stripParen) {
            // Strip out all parenthetical statements when requested.
            input = sParenPattern.matcher(input).replaceAll("");
        }
        return sSanitizePattern.matcher(input.toLowerCase()).replaceAll("");
    }
}
