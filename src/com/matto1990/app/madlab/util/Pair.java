package com.matto1990.app.madlab.util;

/**
 * A generic class to store two values in a pair
 * Why was the Pair class only added in 2.0?
 * @author oakesm9
 *
 * @param <F> The first type
 * @param <S> The second type
 */
public class Pair<F, S> {
    /**
     * The first element
     */
    public F first;
    /**
     * The second element
     */
    public S second;
    
    /**
     * Creates a new pair
     * @param requiredFirst The first element
     * @param requiredSecond The second element
     */
    public Pair(F requiredFirst, S requiredSecond) {
        first = requiredFirst;
        second = requiredSecond;
    }
    
    /**
     * A convince method to create a new pair with a given type
     * @param <F> The first type
     * @param <S> The second type
     * @param requiredFirst The first element
     * @param requiredSecond The second element
     * @return A new pair of given type with given elements
     */
    public static <F, S> Pair<F,S> create(F requiredFirst, S requiredSecond) {
        return new Pair<F, S>(requiredFirst, requiredSecond);
    }
}
