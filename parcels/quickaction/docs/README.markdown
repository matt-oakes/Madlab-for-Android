QuickActions: Quickly give users some actions to take
=====================================================
`Description here`

Usage
-----

`Usage information here`

Dependencies
------------
This depends upon the `cwac-parcel` parcel for accessing
project-level resources.

Version
-------
This is version 1.0 of this module.

Demo
----
There is a `demo/` directory containing a demo project. If you
have the Android Parcel Project client installed, you can
run the `ant demo` command to install the requisite parcels into
the demo project and install the resulting APK into your
attached emulator or device.

License
-------
The code in this project is licensed under the New BSD License,
per the terms of the included LICENSE file.

Who Made This?
--------------
Originally by [qberticus][q]
Modified by [lorenz][l]
Packaged by [Matt Oakes][mo]

[q]: http://code.google.com/p/simple-quickactions/
[l]: http://www.londatiga.net/
[mo]: http://www.matto1990.com/
